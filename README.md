# myfloorballclub-flutter

Club management web-app. Specifically concerned for floorball teams and their managers, coaches and representatives.


## Features

- [x] Add player in list
- [x] Show all players
- [ ] Delete player from list
- [ ] Player form (main data insertion - name, surname, birthdate)
- [ ] View player details (main data + documents statuses) - Admin commands (add/delete/edit)
- [ ] Search bar (find a player)
- [ ] Local storage integration
- [ ] Login/Sign in
- [ ] Deploy
RELEASE 1.0
- [ ] User permissions (admin - guest)
- [ ] Firebase integration (for authentication and database)
- [ ] Manage more clubs (add/edit/delete)
- [ ] Calendar (admin - guest)
- [ ] Coach board


### Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

import 'package:flutter/material.dart';
import 'package:flutteranna_first_app/models/player.dart';

class PlayerTile extends StatelessWidget{ //a stateless should have constants (be immutable) - add final
  //fun to define widget design
  final Player player;

  const PlayerTile(this.player, {Key? key}) : super(key: key); //specific Key: recreates the same Widget

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: const FlutterLogo(size: 56.0),
        title: Text('${player.name} ${player.surname}'),
        subtitle: Text('${player.birthdate}'),
        trailing: const Icon(Icons.more_vert),
      ),
    );
  }

}
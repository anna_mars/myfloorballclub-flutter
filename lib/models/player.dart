
    //file names written in lowercase
    //classes in java syntax: first letter is capitalized es. FlutterApp

    class Player {
      String name;  //Constructor needs to be declared afterwards
      String surname;
      DateTime birthdate;
      String address;
      String email;
      String phone;

      //state + variable, add "late" to initialize later
      late MembershipState fiufMembership;
      late MembershipState shcMembership;
      late MembershipState uispMembership;
      late MedicalState medicalCertState;

      //all constructors declared inside class "Player"
      Player(
      this.name,
      this.surname,
      this.birthdate,
      this.address,
      this.email,
      this.phone,
      this.fiufMembership,
      this.shcMembership,
      this.uispMembership,
      this.medicalCertState);

      Player.beginner(
          this.name,
          this.surname,
          this.birthdate,
          this.address,
          this.email,
          this.phone,
          ){
        fiufMembership = MembershipState.empty;
        shcMembership = MembershipState.empty;
        uispMembership = MembershipState.empty;
        medicalCertState = MedicalState.empty;
      }
    }
    //enum stands for "enumeration" and introduces a list
    //every value has an index (first is 0)
    enum MembershipState {
      empty,
      active,
      expired
    }

    enum MedicalState {
      empty,
      active,
      expiring,
      expired
    }